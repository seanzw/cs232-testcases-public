/*
Check control flow over a if else statement
make sure that this callsite has parameter passback

*/

class Main
{
    public static void main (String[] args)
    {
        A temp;
        A res;
        if(1 < 1)
        {
            temp = new A();
            res = temp.returnSelf(temp);
        }
        else
        {
            temp = new B();
            res = temp.returnSelf(temp);
        }
        System.out.println(res.invoke());
    }
}

class A
{
    A field;
    public A returnSelf(A arg)
    {
        int t;
        t = this.action();
        t = arg.invoke();
        return arg;
    }
    public int invoke()
    {
        return 1;
    }
    public int action()
    {
        field = new A();
        return 0;
    }
}
class B extends A
{
    public A returnSelf(A arg)
    {
        int t;
        t = this.action();
        t = arg.invoke();
        return arg;
    }
    public int invoke()
    {
        return 2;
    }
}