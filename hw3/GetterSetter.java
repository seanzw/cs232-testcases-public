/* If the analysis is context insensitive, 
	the method call in the print statement will have range [-1,1].
	Filename does not reflect the main idea in this testcase.  */

class GetterSetter {
	public static void main(String[] args) {
		A a;
		int b;
		a = new A();
		b = a.returnParam(1-2);
		System.out.println(a.returnParam(2-1));
	}
}

class A {
	public int returnParam(int b) {
		return b;
	}
}